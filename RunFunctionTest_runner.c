#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(RunFunctionTest)
{
	RUN_TEST_CASE(RunFunctionTest, read_file_ReturnTheIndexIfTheFileIsBroken);
	RUN_TEST_CASE(RunFunctionTest, save_ReturnTheIndexIfTheFileIsBroken);

}
