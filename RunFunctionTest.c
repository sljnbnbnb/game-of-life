#include "mainfunvartest.h"
#include <stdio.h>
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP(RunFunctionTest);

//sometimes you may want to get at local data in a module.
//for example: If you plan to pass by reference, this could be useful
//however, it should often be avoided
extern FILE* fp1;
extern FILE* fp2;
extern FILE* fp3;

TEST_SETUP(RunFunctionTest)
{
	//This is run before EACH TEST
	fp1 = fopen("inistate.txt","r");
	fp2 = fopen("save.txt","r");
	fp3 = fopen("xixi.txt","r");
}

TEST_TEAR_DOWN(RunFunctionTest)
{
}

TEST(RunFunctionTest, read_file_ReturnTheIndexIfTheFileIsBroken)
{
	//All of these should pass
	TEST_ASSERT_EQUAL(-1, read_file(fp1));
	TEST_ASSERT_EQUAL(-1, read_file(fp3));

}

TEST(RunFunctionTest, save_ReturnTheIndexIfTheFileIsBroken)
{
	// You should see this line fail in your test summary
	TEST_ASSERT_EQUAL(-1, save(fp2));
	TEST_ASSERT_EQUAL(-1, save(fp3));
	// Notice the rest of these didn't get a chance to run because the line above failed.
	// Unit tests abort each test function on the first sign of trouble.
	// Then NEXT test function runs as normal.
}
