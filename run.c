#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#include "variable.h"

void ini_display();
/*
int read_file(FILE *fp1){
	if(fp1 == NULL){
		printf("Wrong filename.\n");
		return -1;
	}
	char buff1[30];
	fgets(buff1,30,fp1);
	gridsizerow=atoi(buff1);
	char buff2[30];
	fgets(buff2,30,fp1);
	gridsizecol=atoi(buff2);
	char buff3[30];
	fgets(buff3,30,fp1);
	gameStepMax=atoi(buff3);
	for(int i=1;i<=gridsizerow;i++){
		for(int j=1;j<=gridsizecol;j++){
			fscanf(fp1,"%d",&init_state[i][j]);
		}
		fscanf(fp1,"\n");
	}

	return 0;
}
*/
int ask_for_choice(){
	int opt;
	printf("Game option:\n");
	printf("1:Enter game steps\n");
	printf("2:End at the end\n");
	printf("3:Initialize by yourself\n");
	printf(" Option: ");
	int k = scanf("%d",&opt);
	setbuf(stdin,NULL);
	if(k == 0){
		printf("please enter the right option(1 or 2 or 3)\n");
		return -1;
	}
	if(opt<=0 || opt >=4){
		printf("please enter the right option(1 or 2 or 3)\n");
		return -1;
	}
	if(opt == 2){
		gameStep=gameStepMax;
		return 2;
	}
	if(opt==1){
		int zanshi;
		printf("\n");
		printf("Please enter the game step:");
		int k2 = scanf("%d",&zanshi);
		setbuf(stdin,NULL);
		if(k2 == 0){
			printf("please enter a positive interger\n");
			return -1;
		}
		if(zanshi<=0){
			printf("please enter a positive interger\n");
			return -1;
		}
		if(zanshi>gameStepMax){
			printf("The number is out of the step range, please enter a smaller one\n");
			return -1;
		}
		gameStep=zanshi;
		return 1;
	}
	if(opt==3){
		zidingyi=3;
		int col_zdy;
		int row_zdy;
		printf("\n");
		printf("Please enter the gridsize(No more than 10)\n");
		printf("collumn:");
		int k3 = scanf("%d",&col_zdy);
		setbuf(stdin,NULL);
		if(k3 == 0){
			printf("please enter a positive interger(No more than 10)\n");
			return -1;
		}
		if(col_zdy<=0 || col_zdy > 10){
			printf("please enter a positive interger(No more than 10)\n");
			return -1;
		}
		gridsizecol=col_zdy;
		printf("row:");
		int k4 = scanf("%d",&row_zdy);
		setbuf(stdin,NULL);
		if(k4 == 0){
			printf("please enter a positive interger(No more than 10)\n");
			return -1;
		}
		if(row_zdy<=0 || row_zdy > 10){
			printf("please enter a positive interger(No more than 10)\n");
			return -1;
		}
		gridsizerow=row_zdy;

		int bu;
		printf("Please enter the game step:");
		int k5 = scanf("%d",&bu);
		setbuf(stdin,NULL);
		if(k5 == 0){
			printf("please enter a positive interger\n");
			return -1;
		}
		if(bu<=0){
			printf("please enter a positive interger\n");
			return -1;
		}
		gameStep=bu;
		return 1;
	}

}

int Initialization(){
	//int grid[gridsize+2][gridsize+2];
	if(zidingyi==3){
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				init_state[i][j]=0;
			}
		}
	}
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			end_state[i][j]=init_state[i][j];
		}
	}
	return 0;
}

void liveChange(int i,int j){
	int c1=0;
	int c2=0;
	int c3=0;
	int c4=0;
	int c5=0;
	int c6=0;
	int c7=0;
	int c8=0;
	int neighbor;
	if(end_state[i-1][j-1]==1){
		c1=1;
	}
	if(end_state[i-1][j]==1){
		c2=1;
	}
	if(end_state[i-1][j+1]==1){
		c3=1;
	}
	if(end_state[i][j-1]==1){
		c4=1;
	}
	if(end_state[i][j+1]==1){
		c5=1;
	}
	if(end_state[i+1][j-1]==1){
		c6=1;
	}
	if(end_state[i+1][j]==1){
		c7=1;
	}
	if(end_state[i+1][j+1]==1){
		c8=1;
	}
	neighbor=c1+c2+c3+c4+c5+c6+c7+c8;
	if(neighbor<2 || neighbor>3 ){
		save_state[i][j]=0;
	}
	if(neighbor==2 || neighbor==3){
		save_state[i][j]=1;
	}
}

void deadChange(int i, int j){
	int c1=0;
	int c2=0;
	int c3=0;
	int c4=0;
	int c5=0;
	int c6=0;
	int c7=0;
	int c8=0;
	int neighbor;
	if(end_state[i-1][j-1]==1){
		c1=1;
	}
	if(end_state[i-1][j]==1){
		c2=1;
	}
	if(end_state[i-1][j+1]==1){
		c3=1;
	}
	if(end_state[i][j-1]==1){
		c4=1;
	}
	if(end_state[i][j+1]==1){
		c5=1;
	}
	if(end_state[i+1][j-1]==1){
		c6=1;
	}
	if(end_state[i+1][j]==1){
		c7=1;
	}
	if(end_state[i+1][j+1]==1){
		c8=1;
	}
	neighbor=c1+c2+c3+c4+c5+c6+c7+c8;
	if(neighbor == 3){
		save_state[i][j]=1;
	}
}

int checkEnd(){
	int check = 0;
	for(int i=0;i<=gridsizerow;i++){
		for(int j=0;j<=gridsizecol;j++){
			if(end_state[i][j]==0){
				check = check + 1;
			}
		}
	}
	if(check == gridsizerow*gridsizecol){
		return 1;
	}
	return 0;
}

int change(){
	for(int k=0;k<gameStep;k++){
		for(int i=1;i<=gridsizerow;i++){
			for(int j=1;j<=gridsizecol;j++){
				if(end_state[i][j]==1){
					liveChange(i,j);
				}
				if(end_state[i][j]==0){
					deadChange(i,j);
				}
			}
		}
		for(int i=0;i<10;i++){
			for(int k=0;k<10;k++){
				end_state[i][k]=save_state[i][k];
			}
		}
		ini_display();
		SDL_Delay(1000);
		//test
		/*
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				printf("%d ",end_state[i][j]);
			}
			printf("\n");
		}
		printf("\n");*/
		if(checkEnd()==1){
			return 0;
		}
	}
	return 1;

}
