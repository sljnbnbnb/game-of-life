#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#include "variable.h"
#include "mainfunvar.h"


void ini_display(){
	SDL_Window *window = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
	}
	atexit(SDL_Quit); // set for clean-up on exit
	SDL_CreateWindowAndRenderer(max_size_l, max_size_w, 0, &window, &renderer);
	SDL_SetWindowTitle( window, "Circles eat squares");
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
	black_square = SDL_LoadBMP("images/black.bmp");
	white_square = SDL_LoadBMP("images/white.bmp");
	black_texture = SDL_CreateTextureFromSurface(renderer, black_square);
	white_texture = SDL_CreateTextureFromSurface(renderer, white_square);
	SDL_Rect rect;
	int da;
	if(gridsizerow >= gridsizecol){
		da = gridsizerow;
	}
	if(gridsizerow < gridsizecol){
		da = gridsizecol;
	}
	int danyuan;
	danyuan = max_size_l/da;
	for (int i = 1; i <= gridsizerow; i++) {
		for (int j = 1; j <= gridsizecol; j++) {
			rect.h = max_size_l/da;
			rect.w = max_size_l/da;
			rect.x = (j-1) * danyuan;
			rect.y = (i-1) * danyuan;
			if(end_state[i][j] == 1) {
				SDL_RenderCopy(renderer,black_texture, NULL, &rect);
			}
			if(end_state[i][j] == 0){
				SDL_RenderCopy(renderer,white_texture, NULL, &rect);
			}

		}
	}
	SDL_RenderPresent(renderer);
}


int main(int argc,char*argv[]){
	int delay = 5000;
	FILE*fp1;
	fp1= fopen(argv[1],"r");
	read_file(fp1);
	fclose(fp1);
	FILE*fp2 = fopen(argv[2],"w");
	int choice;
	choice = ask_for_choice();
	if(choice == -1){
		choice = ask_for_choice();
	}
	Initialization();
	if(zidingyi != 3){
		//Initialization();
		change();
		ini_display();
		SDL_Delay( delay );
		save(fp2);
		fclose(fp2);
		return 0;
	}
	if(zidingyi == 3){

		int da;
		if(gridsizerow >= gridsizecol){
			da = gridsizerow;
		}
		if(gridsizerow < gridsizecol){
			da = gridsizecol;
		}
		ini_display();
		SDL_Event et;
		while(true){
			SDL_WaitEvent(&et);
			if(et.type == SDL_MOUSEBUTTONDOWN){
				float zbx,zby;
				int zuobiaox,zuobiaoy;
				zbx=et.button.x;
				zby=et.button.y;
				zuobiaox=zbx/(max_size_l/da)+1;
				zuobiaoy=zby/(max_size_l/da)+1;
				if(end_state[zuobiaoy][zuobiaox]==0){
					end_state[zuobiaoy][zuobiaox]=1;
				}
				else if(end_state[zuobiaoy][zuobiaox]==1){
					end_state[zuobiaoy][zuobiaox]=0;
				}
				SDL_Rect rect;
				int danyuan;
				danyuan = max_size_l/da;
				for (int i = 1; i <= gridsizerow; i++) {
					for (int j = 1; j <= gridsizecol; j++) {
						rect.h = max_size_l/da;
						rect.w = max_size_l/da;
						rect.x = (j-1) * danyuan;
						rect.y = (i-1) * danyuan;
						if(end_state[i][j] == 1) {
							SDL_RenderCopy(renderer,black_texture, NULL, &rect);
						}
						if(end_state[i][j] == 0){
							SDL_RenderCopy(renderer,white_texture, NULL, &rect);
						}

					}
				}
				SDL_RenderPresent(renderer);
			}
			if(et.type == SDL_KEYUP){
				if (et.key.keysym.sym == SDLK_SPACE) {
                			break;
            			}
			}
		}
		/*
		printf("test\n");
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				printf("%d ",end_state[i][j]);
			}
			printf("\n");
		}
		printf("\n");*/
		change();
		ini_display();
		SDL_Delay(2000);
		save(fp2);
		fclose(fp2);
		return 0;
	}
}
